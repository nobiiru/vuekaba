import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import axios from 'axios'
import { createRouter } from './router'
import { createStore } from './store'
import { sync } from 'vuex-router-sync'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios);
Vue.config.productionTip = false;

export function createApp () {
  // create store and router instances
  const store = createStore()
  const router = createRouter()

  // sync the router with the vuex store.
  // this registers `store.state.route`
  sync(store, router)

  // create the app instance.
  // here we inject the router, store and ssr context to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
  });

  // expose the app, the router and the store.
  // note we are not mounting the app here, since bootstrapping will be
  // different depending on whether we are in a browser or on the server.
  return { app, router, store }
}
