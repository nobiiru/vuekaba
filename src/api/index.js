import axios from 'axios';

const LATENCY = 16
export const api = axios.create({
    baseURL: '/api',
})

export function getThreadsByCode(board){
  return api.get('/' + board + '/index.json').then((response)=> {
    let data = [];
    response.data.threads.map(function (value){
      data.push(value.thread_num)
    });
    return data;
  })

}

export function fetchThreads (board, ids) {
  return Promise.all(ids.map(id => fetchThread(board, id)))
}

export function fetchThread (board, id) {
  return api.get('/' + board + '/res/' + id + '.json').then((response) => {
    return response.data;
  })
}



export function watchList(cb, type){

  let first = true
  const ref = api.child(`${type}stories`)
  const handler = snapshot => {
    if (first) {
      first = false
    } else {
      cb(snapshot.val())
    }
  }
  ref.on('value', handler)
  return () => {
    ref.off('value', handler)
  }

}


/*
export function getAllThreads (cb, board) {
  let i = 0;
  api.get('/' + board  + '/index.json').then((response)=> {
    response.data.threads.map(function (value) {
      if (data.length < 5){
        data.push({
          id: i++,
          threadID: value.thread_num,
          threadName: value.posts[0].comment.slice(0, 100)
          //timestamp
          //value
          //authorName
        })
      }

      cb(data)
    })})
    .catch(error => {
      //router.push({name: "yourroutename"})
      window.location.href = "/";
    });


}

export function createMessage ({ text, thread }, cb) {
  const timestamp = Date.now()
  const id = 'm_' + timestamp
  const message = {
    id,
    text,
    timestamp,
    threadID: thread.id,
    threadName: thread.name,
    authorName: 'Evan'
  }
  setTimeout(function () {
    cb(message)
  }, LATENCY)
}
*/
