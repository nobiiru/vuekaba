import Vue from 'vue'
import Router from 'vue-router'
import ItemList from '@/views/ItemList'

Vue.use(Router)

const ChooseBoard = id => () => import('../views/BoardChooser').then(m => m.default(id))

export function createRouter () {
  return new Router({
    mode: 'history',
    fallback: false,
    routes: [
      {path: '/:id', name: 'ItemList', component: ChooseBoard()},
    ]
  })
}
