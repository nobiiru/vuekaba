import Vue from 'vue';
import Vuex from 'vuex';
import getters from './getters'
import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex);

export function createStore() {
  return new Vuex.Store({
    state: {
      currentThreadID: null,
      boards: {
        /*
        id: {
          id,
          name,
          messages: [...ids],
          lastMessage
        }
        */
      },
      posts: {
        /*
        id: {
          id,
          threadId,
          threadName,
          authorName,
          text,
          timestamp,
          isRead
        }
        */
      }
    },
    getters,
    actions,
    mutations,
  })
}

