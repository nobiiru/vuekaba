import {
  getThread,
  fetchThreads,
  getThreadsByCode
} from '../api'

export default {
  GET_ALL_THREADS: ({ commit, dispatch, state }, { code }) => {
    commit('SET_ACTIVE_TYPE', { code })
    return getThreadsByCode(code)
      .then(ids => commit('SET_LIST', { code, ids }))
      .then(console.log(1))
      .then(() => dispatch('ENSURE_ACTIVE_THREADS'))
  },


  ENSURE_ACTIVE_THREADS: ({ dispatch, getters }) => {
    console.log(getters.activeIds)
    return dispatch('FETCH_THREADS', {
      ids: getters.activeIds
    })
  },

  FETCH_THREADS: ({ commit, state }, { ids }) => {
    const now = Date.now()

/*
    ids = ids.filter(id => {
      const item = state.items[id]
      if (!item) {
        return true
      }
      else if (now - item.__lastUpdated > 1000 * 60 * 3) {
        return true
      }
      return false
    })*/

    if (ids.length) {
      return fetchThreads(ids).then(items => commit('SET_ITEMS', { items }))
    } else {
      return Promise.resolve()
    }
  },
}

/*
export const getAllThreads = ({ commit }, board) => {
  api.getAllThreads(messages => {
    commit('receiveAll', messages)
  }, board)
}
*/
/*
export const sendMessage = ({ commit }, payload) => {
  api.createMessage(payload, message => {
    commit('receiveMessage', message)
  })
}

export const switchThread = ({ commit }, payload) => {
  commit('switchThread', payload)
}
*/
